#ifndef PROCESSING_HPP
#define PROCESSING_HPP

#include <vector>

#define LOW -32768
#define HIGH 32767

using std::vector;

namespace processing{
	short clamp(const double n, const short low, const short high){
		if (n < low)
			return low;
		if (n > high)
			return high;

		return n;
	}

	short mix(const short n1, const short n2, const double wet=1){
		if (wet == 0)
			return n1;

		if (wet == 1)
			return n2;

		return (n1 * wet) + (n2 * (1 - wet));
	}

	/* EFFECTS */

	void hard_clip(vector <std::pair <short, short>> &samples, const double wet=1, const double strength=0.1){
		for (std::pair<short, short> &s : samples){
			s.first = mix(s.first, clamp(s.first * (1 + strength), LOW, HIGH), wet);
			s.second = mix(s.second, clamp(s.second * (1 + strength), LOW, HIGH), wet);
		}
	}

	void compressor(vector <std::pair <short, short>> &samples, const double wet=1){
		
	}
}

#endif // PROCESSING_HPP
