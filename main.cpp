#include <iostream>
#include <bitset>
#include <vector>
#include <fstream>
#include <sstream>

#include "processing.hpp"

using std::string;
using std::vector;

void usage(){
	std::cout << "Usage: epoxy [audio file 16bit signed wav] [samplerate hz]\n";
}

int main(int argc, char *argv[]){
	if (argc <= 2){
		usage();
		return 1;
	}

	const string filename = argv[1];

	std::ifstream f;
	f.open(filename, std::ios_base::in | std::ios_base::binary);

	if (!f.is_open()){
		std::cerr << "Couldn't open file: " << filename << "\n";
		return 2;
	}

	// TODO: Fix this hack
	// Skip header
	string header;
	char c;
	while (f.get(c)){
		header += c;

		if (header.size() < 4)
			continue;

		if (header.substr(header.size()-4, 4) == "data"){
			f.get(c);
			f.get(c);
			f.get(c);
			f.get(c);
			f.get(c);
			break;
		}
	}

	// TODO Determine size and set that here, then just index later
	// Load entire sample data into memory

	vector <std::pair<short, short>> samples;

	string sampleData(4, '\0');

	while (f.read(&sampleData[0], 4)){
		short s1 = (sampleData[1] & 0xff) | sampleData[0] << 8;
		short s2 = (sampleData[3] & 0xff) | sampleData[2] << 8;
		samples.push_back(std::make_pair(s1, s2));
	}

	processing::hard_clip(samples, 1, 0.3);

	for (std::pair<short, short> sample : samples){
		std::cout << char(sample.first >> 8) << char(sample.first & 0xff);
		std::cout << char(sample.second >> 8) << char(sample.second & 0xff);
	}

	f.close();
}
